#!/bin/sh

chmod -R -x=rX,u+w .
find -type f -path '*.sh' -print0 |xargs -0 chmod =rx,u+w

if [ -s '.exec' ]; then
    chmod =rx,u+w `xargs -a .exec`
fi
